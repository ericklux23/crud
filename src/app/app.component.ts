import { Component } from '@angular/core';
import { Usuario } from './models/usuario';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    usuarioArray: Usuario[] = [
    {id: 1, name: "Shinobu", country: "Guatemala"},
    {id: 2, name: "Tamayo", country: "Guatemala"},
    {id: 3, name: "Douma", country: "Guatemala"}
    ];

    selectedUsuario: Usuario = new Usuario();

    openForEdit(usuario: Usuario){
      this.selectedUsuario = usuario;
    }

    addOrEdit(){
    if(this.selectedUsuario.id === 0){
      this.selectedUsuario.id = this.usuarioArray.length + 1;
      this.usuarioArray.push(this.selectedUsuario);
      }
      this.selectedUsuario = new Usuario();
      }

      delete(){
        if(confirm('Estas seguro que quieres eliminarlo')){
        this.usuarioArray = this.usuarioArray.filter(x => x != this.selectedUsuario)
        this.selectedUsuario = new Usuario;
        }
      }

}
